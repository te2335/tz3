[![pipeline status](https://gitlab.com/te2335/tz3/badges/main/pipeline.svg)](https://gitlab.com/te2335/tz3/-/commits/main)

# ТЗ3

Мое решение (functions.py) включает в себя реализацию на Python простейшей программы, которая считывает из файла числа, а далее отдельными функциями ищет среди этих чисел минимальное число, максимальное число, считает их общую сумму и произведение. 

Для этой программы подготовлены тесты:

- проверяющие корректность работы функций поиска минимума (min-pytest) и максимума (max-pytest), функций сложения (sum-pytest) и умножения (multi-pytest) [все в модуле test_functions.py]
- проверяющие скорость работы программы при увеличении размера входного файла (timesize-pytest) [в модуле test_timeandsize.py]
- осуществляющие бенчмаркинг функций (измеряется суммарное время выполнения всех функций через pytest-benchmark) [в test_benchmarking.py]

Скрипт timesize_visual.py просто для души, он делает то же самое, что и timesize-pytest job, только показывает зависимость на графике.

## Usage

В файле filewithnums.txt: 1 4 2 3

Минимальное: 1

Максимальное: 4

Сумма: 10 (1+2+3+4)

Произведение: 24 (1*2*3*4)

```python
num_list = read_data('filewithnums.txt')
# returns 1
find_min(num_list)

# returns 1
find_max(num_list)

# returns 10 (1+2+3+4)
sum_of_nums(num_list)

# returns 24 (1*2*3*4)
multi_of_nums(num_list)
```
