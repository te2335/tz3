# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 15:18:29 2021

@author: nt_o
"""
import pytest

def test_time():
    
    from tabulate import tabulate    
    from random import randint
    import time
    from functions import find_min, find_max, sum_of_nums, multi_of_nums
    
    def random_int_list(length, start, end):
        result = [randint(start, end) for i in range(length)]
        return result
    
    # декоратор для подсчета времени выполнения
    def test_time(tested_function):
        def timecount(li):
            start_time = time.perf_counter()
            return_value = tested_function(li)
            end_time = time.perf_counter ()
            return return_value, end_time - start_time
        return timecount
    
    find_min = test_time(find_min)
    find_max = test_time(find_max)
    sum_of_nums = test_time(sum_of_nums)
    multi_of_nums = test_time(multi_of_nums)
    
    min_data = [['Длина списка', 'Время на поиск минимума (сек.)']]
    max_data = [['Длина списка', 'Время на поиск максимума (сек.)']]
    sum_data = [['Длина списка', 'Время на подсчет суммы (сек.)']]
    multi_data = [['Длина списка', 'Время на подсчет произведения (сек.)']]
    
    curr_length = 1000
    
    print('Testing started.')
    print()
    
    # генерирую рандомные списки длины 1000, 10000, 100000, 1000000, 10000000
    # с элементами от -100 до 100

    for _ in range(5):
        test_list = random_int_list(curr_length, -100, 100)
       
        minimum = find_min(test_list)
        min_data.append([curr_length, minimum[1]])
        
        maximum = find_max(test_list)
        max_data.append([curr_length, maximum[1]])
        
        summa = sum_of_nums(test_list)
        sum_data.append([curr_length, summa[1]])
        
        multiplication = multi_of_nums(test_list)
        multi_data.append([curr_length, multiplication[1]])
        
        curr_length *= 10
    
    # печатаю таблички
    print(tabulate(min_data))
    print()
    print(tabulate(max_data))
    print()
    print(tabulate(sum_data))
    print()
    print(tabulate(multi_data))
    print()
    print('Execution completed.')
