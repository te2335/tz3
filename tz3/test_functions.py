# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 11:13:47 2021

@author: nt_o
"""
import pytest
from random import randint
import math
from functions import find_min, find_max, sum_of_nums, multi_of_nums


def random_int_list(length, start, end):
    result = [randint(start, end) for i in range(length)]
    return result


def test_stress_find_min():
    
    for i in range(10000):
        test_list = random_int_list(1000, -100, 100)
        assert min(test_list) == find_min(test_list)


def test_stress_find_max():
    
    for i in range(10000):
        test_list = random_int_list(1000, -100, 100)
        assert max(test_list) == find_max(test_list)


def test_stress_sum_of_nums():
    for i in range(10000):
        test_list = random_int_list(1000, -100, 100)
        assert sum(test_list) == sum_of_nums(test_list)


def test_stress_multi_of_nums():
    for i in range(10000):
        test_list = random_int_list(1000, -100, 100)
        assert math.prod(test_list) == multi_of_nums(test_list)
        
