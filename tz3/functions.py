# -*- coding: utf-8 -*-
"""
Created on Sun Dec 11 10:23:16 2021

@author: nt_o
"""
def read_data(file_name):
    with open(file_name, 'r', encoding='utf-8') as f:
        data = []
        for line in f:
            for num in line.split():
                data.append(int(num))
    return data


def find_min(li):
    curr_min = li[0]
    for num in li[1:]:
        if num < curr_min:
            curr_min = num
    return curr_min


def find_max(li):
    curr_max = li[0]
    for num in li[1:]:
        if num > curr_max:
            curr_max = num
    return curr_max


def sum_of_nums(li):
    summa = 0
    for num in li:
        summa += num
    return summa


def multi_of_nums(li):
    multi = 1
    for num in li:
        multi *= num
    return multi

if __name__ == '__main__':
    
    # это для примера
    num_list = read_data('filewithnums.txt')
    print(num_list)
    
    print(find_min(num_list))
    
    print(find_max(num_list))
    
    try:
        print(sum_of_nums(num_list))
    except: # сюда разные ошибки могут полететь, но и ошибка переполнения тоже (что самое главное)
        print('SUM OF ELEMENTS: overflow error.')
    
    try:
        print(multi_of_nums(num_list))
    except:
        print('MULTIPLICATION OF ELEMENTS: overflow error occured.')
        
