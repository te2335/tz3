# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 14:54:31 2021

@author: nt_o
"""
from random import randint
import time
import matplotlib.pyplot as plt
from functions import find_min, find_max, sum_of_nums, multi_of_nums

def random_int_list(length, start, end):
    result = [randint(start, end) for i in range(length)]
    return result

# декоратор для подсчета времени выполнения
def test_time(tested_function):
    def timecount(li):
        start_time = time.perf_counter()
        return_value = tested_function(li)
        end_time = time.perf_counter ()
        return return_value, end_time - start_time
    return timecount

find_min = test_time(find_min)
find_max = test_time(find_max)
sum_of_nums = test_time(sum_of_nums)
multi_of_nums = test_time(multi_of_nums)

len_data, min_data, max_data, sum_data, multi_data = [], [], [], [], []

curr_length = 1000

print('Testing started.')

for _ in range(5):
    test_list = random_int_list(curr_length, -100, 100)
   
    minimum = find_min(test_list)
    min_data.append(minimum[1])
    
    maximum = find_max(test_list)
    max_data.append(maximum[1])
    
    summa = sum_of_nums(test_list)
    sum_data.append(summa[1])
    
    multiplication = multi_of_nums(test_list)
    multi_data.append(multiplication[1])
    
    len_data.append(curr_length)
    curr_length *= 10


plt.plot(len_data, min_data, label="minimum")
plt.plot(len_data, max_data, label="maximum")
plt.plot(len_data, sum_data, label="sum")
plt.plot(len_data, multi_data, label="multiplication")
plt.xlabel('list length')
plt.ylabel('execution time')
plt.legend()
plt.grid(True)

print('Execution completed.')
