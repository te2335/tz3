# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 19:48:56 2021

@author: nt_o
"""
import pytest

from functions import find_min, find_max, sum_of_nums, multi_of_nums
from test_functions import random_int_list

def all_functions():
    test_list = random_int_list(10000, -100, 100)
    minimum = find_min(test_list)
    maximum = find_max(test_list)
    summa = sum_of_nums(test_list)
    multiplication = multi_of_nums(test_list)
    return minimum, maximum, summa, multiplication

def test_benchmarking_all_functions(benchmark):
    print('Tested list length is 10000')
    result = benchmark(all_functions)
